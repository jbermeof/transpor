create database titulacion;

use titulacion;

create table periodo(
	
idperiodo 	int 	auto_increment,
	
fechaini	datetime,
	
fechafin	datetime,
	
estado	char(1),
	
observacion varchar(100),

primary key (idperiodo)
);